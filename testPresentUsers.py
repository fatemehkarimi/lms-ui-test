import unittest
from selenium import webdriver

# local
from __init__ import LoginBasedTestCase
from setting import USER_FIRSTNAME, USER_LASTNAME

class PresentUsersTestCase(LoginBasedTestCase):
    def test_user_name_is_present(self):
        present_user = self.driver.find_element_by_partial_link_text(
            USER_FIRSTNAME + " " + USER_LASTNAME)
        self.assertTrue(present_user)


if __name__ == '__main__':
    unittest.main()
