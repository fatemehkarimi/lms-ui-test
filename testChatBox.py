import unittest

# local
from __init__ import LoginBasedTestCase
from helper.__init__ import (
    open_chat_box,
    open_message_group,
    open_contact_messages,
    read_contact_messages,
)

# TESTCASE PARAMETERS
MESSAGE_GROUP = 'خصوصی'
CONTACT = 'علی محمد دوست'
MESSAGE_BODY = """دانشجویان محترملطفا تمرین شماره 2 تجزیه و تحلیل را دریافت و حل آن را تا 29 مهر تحویل نمایید."""
class ReadChatBoxMessageTestCase(LoginBasedTestCase):
    def test_read_message(self):
        success = open_chat_box(self.driver)
        self.assertTrue(success)

        success = open_message_group(self.driver, MESSAGE_GROUP)
        self.assertTrue(success)

        success = open_contact_messages(self.driver, CONTACT)
        self.assertTrue(success)

        messages = read_contact_messages(self.driver, CONTACT)
        success = False

        for msg in messages:
            if MESSAGE_BODY in msg:
                success = True
                break
        
        self.assertTrue(success)


if __name__ == '__main__':
    unittest.main()