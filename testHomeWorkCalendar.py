import unittest

# local
from __init__ import LoginBasedTestCase
from helper.utility import get_month_index
from helper.__init__ import (
    goto_date_on_calendar,
    goto_day_page,
    get_number_of_events_in_day_page)

# TESTCASE PARAMETERS
YEAR = 1399
MONTH = 'تیر'
DAY = 10
EXPECTED_NUMBER_OF_HOMEWORKS = 8

class HomeWorkCalendarTestCase(LoginBasedTestCase):
    def test_calendar(self):
        success = goto_date_on_calendar(self.driver, YEAR, MONTH, DAY)
        self.assertTrue(success, "FAIL: cannot open calendar")

        success, day_type = goto_day_page(self.driver, DAY)
        self.assertTrue(success, "FAIL: cannot goto day page")

        if day_type == "simple": # no event is on day
            self.assertEqual(EXPECTED_NUMBER_OF_HOMEWORKS, 0,
                                "FAIL: number of events does not match")
        else:
            n_events = get_number_of_events_in_day_page(self.driver)
            self.assertEqual(n_events, EXPECTED_NUMBER_OF_HOMEWORKS,
                                "FAIL: number of events does not match")


if __name__ == "__main__":
    unittest.main()