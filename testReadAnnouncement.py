import unittest

# local
from __init__ import LoginBasedTestCase
from helper.__init__ import (
    goto_course,
    goto_folder,
    goto_message_in_forum,
    read_message_in_forum,
)

# TESTCASE PARAMETERS
YEAR = 1398
SEMESTER = 'بهار'
COURSE = 'مهندسی نرم‌افزار 2'
FORUM_NAME = 'تالار اعلانات'
MESSAGE_TITLE = 'اطلاعیه مهم تحویل پروژه'
MESSAGE_BODY = """سلام 
به دلیل تعطیلی دانشگاه در هفته آینده، موعد تحویل کلیه موارد و جلسه نهایی تحویل پروژه یک هفته تمدید شد، نهایتا تا یکشنبه 26 مرداد.
سالم و موفق باشید
"""

class ReadAnnoouncementTestCase(LoginBasedTestCase):
    def test_read_announcement_message(self):
        success = goto_course(self.driver, YEAR, SEMESTER, COURSE)
        self.assertTrue(success)

        success = goto_folder(self.driver, FORUM_NAME)
        self.assertTrue(success)

        success = goto_message_in_forum(self.driver, MESSAGE_TITLE)
        self.assertTrue(success)

        message_body = read_message_in_forum(self.driver)
        if not message_body or MESSAGE_BODY not in message_body:
            raise AssertionError("FAIL: message bodies did not match")


if __name__ == '__main__':
    unittest.main()