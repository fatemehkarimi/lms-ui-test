import unittest

# local
from __init__ import LoginBasedTestCase
from helper.__init__ import goto_course

# TESTCASE PARAMETERS
YEAR = 1398
SEMESTER = 'بهار'
COURSE = 'پروژه'

class GotoCourseTestCase(LoginBasedTestCase):
    def test_course_page_is_correct(self):
        success = goto_course(self.driver, YEAR, SEMESTER, COURSE)
        self.assertTrue(success)

        header = self.driver.find_element_by_tag_name('h1')
        if COURSE not in header.text:
            raise AssertionError("FAIL: testGotoCouse failed, course page does not match")



if __name__ == '__main__':
    unittest.main()