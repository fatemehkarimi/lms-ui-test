# UI testcases for IUT learning management system with selenium
## 1. Item Introduction
This repository contains some ui testcases for [LMS](https://lms.iut.ac.ir/) . all testcases 
are written with [Selenium](https://www.selenium.dev/) framework in python.

## 2. Install
Install requirements via `pip install -r requirements.txt`

# 3. Running tests
simply use `python3 [test_name]` to run a test. for example, type `python3 testChatBox.py` to run this test.


