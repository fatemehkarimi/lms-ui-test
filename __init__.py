import unittest
from selenium import webdriver

# local
from helper.__init__ import new_driver, login, logout, goto_course
from setting import LOGIN_USERNAME, LOGIN_PASSWORD

class LoginBasedTestCase(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        self.username = LOGIN_USERNAME
        self.password = LOGIN_PASSWORD
        super().__init__(*args, **kwargs)

    def setUp(self):
        self.driver = new_driver()
        success_login = login(self.driver, self.username, self.password)
        self.assertTrue(success_login)

    def tearDown(self):
        logout(self.driver)
        self.driver.quit()

