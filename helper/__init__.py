from time import sleep

from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

# local
from setting import BASE_URL, LOAD_PAGE_TIMEOUT
from . utility import get_month_index

def new_driver():
    options = webdriver.ChromeOptions()
    options.add_argument("--start-maximized")
    options.add_argument("enable-automation")
    options.add_argument("--no-sandbox")
    options.add_argument("--disable-infobars")
    options.add_argument("--disable-dev-shm-usage")
    options.add_argument("--disable-browser-side-navigation")
    options.add_argument("--disable-gpu")
    options.add_experimental_option("prefs", {
        "download.default_directory": "downloads",
        "download.prompt_for_download": False,
        "download.directory_upgrade": True,
        "safebrowsing_for_trusted_sources_enabled": False,
        "safebrowsing.enabled": False
    })

    driver = webdriver.Chrome(chrome_options=options)
    driver.implicitly_wait(20)
    return driver


def login(driver, username, password):
    driver.get(BASE_URL)
    if driver.title not in "سامانه آموزش مجازی دانشگاه صنعتی اصفهان":
        return False

    login_link = driver.find_element_by_partial_link_text("ورود")
    login_link.click()

    try:
        present_element = EC.presence_of_element_located((By.ID, "loginbtn"))
        WebDriverWait(driver, LOAD_PAGE_TIMEOUT).until(present_element)
    except TimeoutException:
        return False
    
    central_authentication_btn = driver.find_element_by_partial_link_text("احراز هویت مرکزی")
    central_authentication_btn.click()

    try:
        present_element = EC.presence_of_element_located((By.ID, "submit"))
        WebDriverWait(driver, LOAD_PAGE_TIMEOUT).until(present_element)
    except TimeoutException:
        return False

    u_name_field = driver.find_element_by_id("username")
    password_field = driver.find_element_by_id("password")

    u_name_field.send_keys(username)
    password_field.send_keys(password)

    submit_btn = driver.find_element_by_id("submit")
    submit_btn.click()

    if driver.title in "میز کار":
        return True
    return False


def logout(driver):
    dropdown = driver.find_element_by_id("action-menu-toggle-1")
    if not dropdown:
        print("ERROR: failed to logout")
        return

    dropdown.click()
    logout_btn = driver.find_element_by_partial_link_text("خروج از سایت")
    if not logout_btn:
        print("ERROR: failed to logout")
        return
    logout_btn.click()


def goto_course(driver, year, semester, course):
    data_id = str(year) + ('W' if semester == 'بهار' else 'S')
    try:
        semester = driver.find_element_by_xpath(
            f"//fieldset[contains(@class, '{data_id}')]")
    except Exception as e:
        print(f"ERROR: invalid semester {semester} error msg is:", e)
        return False

    if not ('expanded' in semester.get_attribute('class')):
        legend = semester.find_element_by_tag_name('legend')
        legend.click()

    try:
        course_link = driver.find_element_by_xpath(
            f"//a[contains(text(), '{course}')]")
        course_link.click()
    except Exception as e:
        print(f"ERROR: invalid course {course} error msg is:", e)
        return False
    return True


def goto_message_in_forum(driver, title):
    try:
        message = driver.find_element_by_xpath(
            f"//a[contains(@title, '{title}')]")
        message.click()
    except Exception as e:
        print(f"ERROR: invalid message error msg is:", e)
        return False
    return True


def read_message_in_forum(driver):
    try:
        message = driver.find_element_by_xpath(
            "//div[contains(@id, 'post-content')]")
        message.click()
    except:
        print(f"ERROR: no message found in this page. \
            maybe you should call goto_message_in_forum first?")
        return ""
    
    paragraphs = message.find_elements_by_tag_name('p')
    message_body = ''
    for p in paragraphs:
        message_body += p.text
        message_body += '\n'

    return message_body


def open_chat_box(driver):
    try:
        dropdown = driver.find_element_by_id("action-menu-toggle-1")
        dropdown.click()
    except Exception as e:
        print("ERROR: failed to open dropdown. error msg is", e)
        return False

    try:
        message_btn = driver.find_element_by_partial_link_text("پیام")
        message_btn.click()
    except Exception as e:
        print("ERROR: failed to open chat box. error msg is", e)
        return False
    return True
    

def open_message_group(driver, group):
    try:
        group_link = driver.find_element_by_xpath(f"//span[text() = '{group}']")
        group_link.click()
    except Exception as e:
        print("ERROR: no such group. error msg is", e)
        return False
    return True


def open_contact_messages(driver, name):
    try:
        present_element = EC.presence_of_element_located((
            By.XPATH,
            f"//div[@class='d-flex']/strong[contains(text(), '{name}')]"))

        WebDriverWait(driver, LOAD_PAGE_TIMEOUT).until(present_element)
    except TimeoutException:
        return False

    try:
        btn = driver.find_element_by_xpath(f"//div[@class='d-flex']/strong[contains(text(), '{name}')]")
        btn.click()
    except:
        print("ERROR: cannot click on contact. maybe in wrong page?")
        return False
    return True



def read_contact_messages(driver, contact):
    messages = []
    sleep(LOAD_PAGE_TIMEOUT)

    try:
        message_elements = driver.find_elements_by_xpath(
            f"//div[contains(@class, 'message')]/div[contains(@data-region, 'text-container')]"
        )
        print("message_elements = ", message_elements)
    except Exception as e:
        print("ERROR: can't load messages, the error msg is", e)
        return messages

    for msg in message_elements:
        msg_body = ''
        paragraphs = msg.find_elements_by_tag_name('p')
        for p in paragraphs:
            msg_body += p.text
        messages.append(msg_body)

    print(messages)
    return messages


def goto_folder(driver, folder_name):
    try:
        folder = driver.find_element_by_xpath(f"//span[contains(text(), '{folder_name}')]")
        folder.click()
    except Exception as e:
        print("ERROR: no such folder, error msg is", e)
        return False
    return True

# function to take care of downloading file
# author: https://medium.com/@moungpeter/how-to-automate-downloading-files-using-python-selenium-and-headless-chrome-9014f0cdd196
def enable_download_headless(browser,download_dir):
    browser.command_executor._commands["send_command"] = ("POST", '/session/$sessionId/chromium/send_command')
    params = {'cmd':'Page.setDownloadBehavior', 'params': {'behavior': 'allow', 'downloadPath': download_dir}}
    browser.execute("send_command", params)


def download_files(driver, download_dir):
    enable_download_headless(driver, download_dir)
    try:
        download_btn = driver.find_element_by_xpath("//button[text() = 'دریافت کل محتوای پوشه']")
        download_btn.click()
    except Exception as e:
        print("ERROR: cannot downlaod files, error msg is", e)
        return False
    return True


def get_current_date(driver):
    try:
        date = driver.find_element_by_xpath("//a[@title = 'ماه جاری']")
        return date.text
    except Exception as e:
        print("ERROR: cannot read date of website, error msg is", e)
        return ''


def goto_date_on_calendar(driver, year, month, day):
    date = get_current_date(driver).split()
    if not date:
        return False
    
    curr_month, curr_year = date[0], int(date[1])

    curr_month_idx = get_month_index(curr_month)
    month_idx = get_month_index(month)

    if curr_month_idx == -1 or month_idx == -1:
        return False

    # this means that given date is on the current page of calendar
    date_status = 'current'

    if curr_year < year:
        date_status = 'after'
    elif curr_year == year and curr_month_idx < month_idx:
        date_status = 'after'
    elif curr_year == year and curr_month_idx == month_idx:
        date_status = 'current'
    elif curr_year == year and curr_month_idx > month_idx:
        date_status = 'before'
    elif curr_year > year:
        date_status = 'before'

    if date_status == 'before':
        btn_xpath = "//a[@title = 'ماه گذشته']"
    elif date_status == 'after':
        btn_xpath = "//a[@title = 'ماه بعد']"

    maximum_allowed_number_of_click = abs(curr_month_idx - month_idx)
    while get_current_date(driver).split() != [month, str(year)]:
        loading_icon = WebDriverWait(driver, LOAD_PAGE_TIMEOUT).until(
            EC.invisibility_of_element_located((By.XPATH,
            "//div[contains(@id, 'calendar-month')]/div/span[contains(@class, 'hidden')]"))
        )
        try:
            btn = WebDriverWait(driver, LOAD_PAGE_TIMEOUT).until(
                EC.element_to_be_clickable((By.XPATH, btn_xpath)))
            
            if maximum_allowed_number_of_click > 0:
                btn.click()
                maximum_allowed_number_of_click -= 1
        except Exception as e:
            print("ERROR: cannot goto previous date, error msg is", e)
            return False
    return True


# call goto_date_on_calendar before this
def goto_day_page(driver, day):
    try:
        #when day has no event it is just an span
        day_span = driver.find_element_by_xpath(
            f"//div[contains(@id, 'calendar-month')]/div/table/tbody/tr/td/span[text() = {day}]")
        return (True, "simple")
    except:
        pass

    try:
        day_link = driver.find_element_by_xpath(
            f"//div[contains(@id, 'calendar-month')]/div/table/tbody/tr/td/a[text() = {day}]"
        )
        day_link.click()
        return (True, "link")
    except Exception as e:
        return (False, None)


def get_number_of_events_in_day_page(driver):
    try:
        events = driver.find_elements_by_xpath("//div[@data-type='event']")
        return len(events)
    except Exception as e:
        print("ERROR: failed to find events. error msg is", e)
        return -1