import os
from time import sleep

PERSIAN_MONTHS = [
    'فروردین',
    'اردیبهشت',
    'خرداد',
    'تیر',
    'مرداد',
    'شهریور',
    'مهر',
    'آبان',
    'آذر',
    'دی',
    'بهمن',
    'اسفند'
]

def clean_dir(dir):
    files = os.listdir(dir)
    
    for f in files:
        os.remove(dir + "/" + f)


def is_file_downloaded(dir, timeout=60):
    counter = 0
    while counter < timeout:
        files = os.listdir(dir)
        if len(files) > 0:
            return True
        counter += 1
        sleep(1)
    return False


def get_month_index(month):
    for i in range(len(PERSIAN_MONTHS)):
        if PERSIAN_MONTHS[i] == month:
            return i
    return -1 # persian month not found