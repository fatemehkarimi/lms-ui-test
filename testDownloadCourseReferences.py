import unittest

# local
from __init__ import LoginBasedTestCase
from helper.__init__ import goto_course, goto_folder, download_files
from helper.utility import clean_dir, is_file_downloaded
from setting import DOWNLOAD_DIR

#TESTCASE PARAMETERS
YEAR = 1398
SEMESTER = 'بهار'
COURSE = 'مهندسی نرم‌افزار 2'
REF_FOLDER = 'مراجع درس'

class downloadReferenceTestCase(LoginBasedTestCase):
    def test_download_refs(self):
        clean_dir(DOWNLOAD_DIR)

        success = goto_course(self.driver, YEAR, SEMESTER, COURSE)
        self.assertTrue(success)

        success = goto_folder(self.driver, REF_FOLDER)
        self.assertTrue(success)

        success = download_files(self.driver, DOWNLOAD_DIR)
        self.assertTrue(success)

        success = is_file_downloaded(DOWNLOAD_DIR)
        self.assertTrue(success)


if __name__ == '__main__':
    unittest.main()
